# platform-gen

## 项目介绍
微同软件代码生成工具IDEA插件

## 版本要求：
IntelliJ IDEA 2018.1 +

## 使用教程

### 安装
#### 提供两种安装方式
##### 1、插件应用市场安装
- 打开IDEA开发工具
- 选择菜单 File -> Settings -> Plugins -> Browse repositories...
- 在输入框搜索 `platform-gen`，然后Install，如下图所示
![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190423/install-gen.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190423/install-gen.png "安装图示")
- Restart

##### 2、本地文件安装
- 打开IDEA开发工具
- 选择菜单 File -> Settings -> Plugins
    - Install plugin from disk...
    - 选择 platform-gen\.idea\platform-gen.zip 文件，然后点击OK
- Restart
### 使用
- 重启IDEA开发工具
- 选择菜单 File -> Settings -> Other Settings -> platform-gen
- 添加项目配置，点击OK
- 使用快捷键 ctrl + shift + alt + y
- 填写表名，生成代码

**效果图：**
- 设置
![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/setting.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/setting.png "设置")
- 操作
![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/gen.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/gen.png "操作")
- 代码
![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/code.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20190329/code.png "代码")


## 官网
[https://fly2you.cn](https://fly2you.cn)

## 后台管理系统演示
* 演示地址：[http://fly2you.cn/platform](http://fly2you.cn/platform)  `账号密码：admin/admin`

* 如何交流、反馈、参与贡献？
    * 官方QQ群：<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=75689ba2797dd88a208446088b029fbdeba87a29315ff2a021a6731f22ef5052"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="微同软件" title="微同软件"></a>
    * 博客：[http://fly2you.cn/mblog](http://fly2you.cn/mblog)
    * git：[https://gitee.com/fuyang_lipengjun/platform](https://gitee.com/fuyang_lipengjun/platform)
    * 基础架构版
        * git：[https://gitee.com/fuyang_lipengjun/platform-framework](https://gitee.com/fuyang_lipengjun/platform-framework)
        * 演示地址：[http://fly2you.cn/platform-framework](http://fly2you.cn/platform-framework)
    * 如需获取项目最新源码，请Watch、Star项目，同时也是对项目最好的支持

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
